import React, { Component } from "react";
import Footer from './components/ui/Footer';
import { ThemeProvider } from '@mui/material/styles';
import { Grid } from '@mui/material';
import Header from './components/ui/Header';
import theme from './components/ui/Theme';
import data from "./data.json";
import Products from "./components/products/Products";
import Filter from './components/Filter/Filter';
import Cart from './components/Cart/Cart';


class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      products: data.products,
      size: "",
      sort: "",
    cartItems: localStorage.getItem("cartItems") 
    ? JSON.parse(localStorage.getItem("cartItems")) 
    : [],
    };
  };

  removeFromCart = (product) => {
    const cartItems = this.state.cartItems.slice();
    this.setState({
      cartItems:cartItems.filter((x) => x._id !== product._id)    
    });
    localStorage.setItem("cartItems", JSON.stringify(cartItems.filter((x) => x._id !== product._id)));
  };

  addToCart = (product) => {
    const cartItems = this.state.cartItems.slice()
    let alreadyInCart = false
    cartItems.forEach(item => {
      if(item._id === product._id) {
        item.count++;
        alreadyInCart = true;
      }
    })
    if(!alreadyInCart) {
      cartItems.push({...product, count: 1})
    }
    this.setState({cartItems})
    localStorage.setItem("cartItems", JSON.stringify(cartItems))
  };

  sortProducts = (event) => {
    const sort = event.target.value;
    this.setState((state) => ({
      sort: sort,
      products: this.state.products.slice().sort((a, b) => (
        sort === "lowest0" ? ((a.price > b.price) ? 1 : -1) :
        sort === "highest" ? ((a.price < b.price) ? 1 : -1) :
        ((a._id < b._id) ? 1 : -1)
      ))
    }));
  }
  render() {
    return (
        <ThemeProvider theme={theme}>
          <Header />
          <Grid container>
            <Grid item xs={9}> 
              <Filter count={this.state.products.length} 
              sort={this.state.sort} 
              sortProducts={this.sortProducts} />
              <Products products={this.state.products} addToCart={this.addToCart} />
            </Grid>
            <Grid item xs={3}>
              <Cart 
              cartItems={this.state.cartItems}
              removeFromCart={this.removeFromCart} />
            </Grid>
          </Grid>
          <Footer />
        </ThemeProvider>
    );
  }
}

export default App;
