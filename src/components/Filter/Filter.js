import React from 'react'
import { makeStyles } from '@mui/styles'
import { Grid, InputLabel, MenuItem, Select, Typography, FormControl } from '@mui/material';

const useStyles = makeStyles((theme) => ({
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120
    },
    productCount: {
        margin: theme.spacing(2,0,0,0)
    }
}));

function Filter({count, sort, sortProducts}) {
    const classes = useStyles();
  return (
    <div stype = {{padding:12}}>
      <Grid container>
        <Grid container items md={6} alignItems="center">
            <Typography variant="body1" className={classes.productCount}>

            {count} Products
            </Typography>
        </Grid>
        <Grid items md={6}>
            <FormControl className={classes.formControl}>
                <InputLabel>
                    Sort Type
                </InputLabel>
                <Select value={sort} onChange={sortProducts}>
                    <MenuItem value=''>Latest</MenuItem>
                    <MenuItem value='lowest'>Lowest</MenuItem>
                    <MenuItem value='highest'>Highest</MenuItem>
                </Select>
            </FormControl>
        </Grid>
      </Grid>
    </div>
  )
}

export default Filter
