import React from 'react'
import {AppBar, Toolbar, Typography, useScrollTrigger} from '@mui/material';
import {makeStyles} from '@mui/styles';

function ElevationScroll(props: Props) {
    const { children } = props;
    const trigger = useScrollTrigger({
      disableHysteresis: true,
      threshold: 0,
    });
  
    return React.cloneElement(children, {
      elevation: trigger ? 4 : 0,
    });
  }

const useStyles = makeStyles((theme) => ({
    toolbarMargin: {
        ...theme.mixins.toolbar
    }
}));

const Header = () => {
    const classes = useStyles();
  return (
    <div>
        <ElevationScroll>
        <AppBar>
            <Toolbar>
                <Typography variant="h5">
                    Shop
                </Typography>
            </Toolbar>
        </AppBar>
        </ElevationScroll>
        <div className={classes.toolbarMargin} />
    </div>
  )
}



export default Header
