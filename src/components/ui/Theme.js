import {createTheme} from '@mui/material/styles';

const shopcartBlue = "#171368";
const shopcartOrange = "#ebc100";
export default createTheme ({
    palette: {
        common: {
            blue: `${shopcartBlue}`,
            orange: `${shopcartOrange}`,
        },
        primary: {
            main: `${shopcartBlue}`,
        },
        secondary: {
            main: `${shopcartOrange}`,
        }
    }
})