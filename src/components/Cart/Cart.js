import React, { Component } from 'react';
import { Typography, Card, CardMedia, CardContent, Button } from '@mui/material';
import { formatPrice } from '../../util';
import { withStyles } from '@mui/styles';

const styles = (theme) => ({
  root: {
    display: 'flex',
    marginTop: '25px'
  },
  details: {
    display: 'flex',
    flexDirection: 'column'
  },
  content: {
    flex: '1 0 auto',
  },
  cover: {
    width: 151,
  },
  cartCount: {
    marginTop: "50px"
  },
  priceBlock: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  totalAmount: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: '20px'
  }
});

class Cart extends Component {
    render() {
        const { classes } = this.props;
        const { cartItems } = this.props;
  return ( 
    <>
        <Typography variant='body1' className={classes.cartCount}>
      {cartItems.length === 0 ? (
      <div> Cart is empty </div>
      ) : (
        <div> You have { cartItems.length } in the cart </div>
      )}
      </Typography>
      <div>
        {cartItems.map((item) => 
          <Card key={item._id} className={classes.root}>
            <CardMedia>
              className={classes.cover} 
              image={item.image} 
              title={item.title}
            </CardMedia>
            <div className={classes.details}>
              <CardContent className={classes.content}>
                <Typography component="div" variant="body1">
                  {item.title}
                </Typography>
                <div className={classes.priceBlock}>
                  <Typography variant="subtitle1" color="textSecondary">
                    {formatPrice(item.price)} x {item.count}
                  </Typography>
                  <Button color="secondary" variant="contained" size="small" onClick={() =>
                     this.props.removeFromCart(item)}>
                      Remove
                  </Button>
                </div>
              </CardContent>
            </div>
          
        <div>
          <div className={classes.totalAmount}>
            <Typography variant='h6'>
              Total: {""}
              {formatPrice(
                cartItems.reduce((a, c) => a + c.price * c.count), 0
                )}
            </Typography>
            <Button variant="contained" size="large" color="secondary">
              Checkout
            </Button>
          </div>
        </div>
        </Card>
        )}
        {cartItems.length !== 0 && (
      <div>
        <div className={classes.totalAmount}>
              <Typography variant='h6'>
                Total:
                {formatPrice(cartItems.reduce((a,c) => a + c.price * c.count, 0)
                )}
              </Typography>
              <Button variant="contained" size="large" color="secondary">
                Checkout
              </Button>
        </div>
      </div>          
      )}
      </div>
    </>
    )
  }
} 

export default withStyles(styles)(Cart)