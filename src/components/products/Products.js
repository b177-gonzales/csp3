import React from 'react';
import { makeStyles } from '@mui/styles';
import {formatPrice} from '../../util';
import { Grid, CardMedia, Typography, CardActions, Button } from '@mui/material';


const useStyles = makeStyles({
    media : {
        height: 500,
        width: '100%'
    },
    link: {
        color: 'white'
    },
    price: {
        fontSize:'1.25rem',
        marginRight: '10px'
    }
})

const Products = ({products, addToCart}) => {
    const classes = useStyles();
  return (
    <div style={{padding: 12}}>
        <Grid container spacing={3}>
            {products.map((product) => (
                <Grid item md={4} key={product._id}>
                    <a className={classes.link} href={'#' + product._id}>
                        <CardMedia 
                            className={classes.media} 
                            image={product.image} 
                            alt={product.title}
                            title={product.title}
                        />
                        <Typography variant="h6" color="primary" component="p">
                            {product.title}
                        </Typography>
                    </a>
                    <CardActions>
                       <Typography className={classes.price} component="div" >
                           {formatPrice(product.price)}
                       </Typography>
                       <Button onClick={() => addToCart(product)} variant="contained" size="large" color="secondary">
                           Add to Cart
                       </Button>
                    </CardActions>
                </Grid>
            ))}
        </Grid>
    </div>
  )
}

export default Products
