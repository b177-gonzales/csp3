const express = require('express');
const bodyParser = require('body-parser');
const shortid = require('shortid');

const app = express();

app.use(bodyParser.json());

mongoose.connect("mongodb+srv://admin2:admin2@wdc028-course-booking.ywk7a.mongodb.net/CSP3?retryWrites=true&w=majority", {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true
});

const Product = mongoose.model(
    "products", 
    new mongoose.Schema({
        _id: {type: String, default: shortid.generate()},
        title: String,
        description: String,
        image: String,
        price: Number,
        availableSizes: {String},
    })
);

app.get("/products", async (req, res) => {
    const products = await Product.find({});
    res.send(products);
});

app.post("/products", async(req, res) => {
    const newProduct = new Product(req.body)
    const savedProduct = await newProduct.save();
    res.send(savedProduct)
});

app.delete("/products/:id", async(req, res) => {
    const deletedProduct = await Product.findbyIdandDelete(req.params.id)
    res.send(deletedProduct);
})

const port = process.env.PORT || 5000
app.listen(port, () => console.log("server at http://localhost:5000"));